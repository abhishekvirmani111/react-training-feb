const List = function(element, listItems) {
  this.pageSize = 10;
  this.currentPage = 1;
  let row, col1, col2, col3, current, final, pageCount;
  current = 0;
  final = 10;
  pageCount = document.getElementById("page-count");

  this.render = function() {
    this.buttonToggle();
    element.innerHTML = "";
    pageCount.innerHTML = this.currentPage;
    const arrayEle = listItems.slice(current, final);
    arrayEle.forEach(item => {
      row = element.insertRow();
      col1 = row.insertCell();
      col2 = row.insertCell();
      col3 = row.insertCell();
      col1.innerHTML = item.id;
      col2.innerHTML = item.name;
      col3.innerHTML = item.email;
    });
  };

  this.navigateFirst = function() {
    current = 0;
    final = 10;
    this.currentPage = 1;
    this.render();
  };

  this.navigateLast = function() {
    final = listItems.length;
    current = Math.floor(listItems.length / 10) * 10;
    if (current === final) {
      current = current - 10;
    }
    this.currentPage = Math.ceil(listItems.length / 10);
    this.render();
  };

  this.navigatePrev = function() {
    final = current;
    current = current - 10;
    this.currentPage = this.currentPage - 1;
    this.render();
  };

  this.navigateNext = function() {
    current = final;
    final = final + 10;
    this.currentPage = this.currentPage + 1;
    this.render();
  };

  this.buttonToggle = function() {
    if (current == 0) {
      document.getElementById("prev").disabled = true;
    } else {
      document.getElementById("prev").disabled = false;
    }
    if (final >= listItems.length) {
      document.getElementById("next").disabled = true;
    } else {
      document.getElementById("next").disabled = false;
    }
  };
};
